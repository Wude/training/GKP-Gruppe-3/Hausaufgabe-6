#include "stdafx.h"

#include "test.h"

// Testdaten (erzeugt aus der Übungsdatenbank des Kurses "Datenbanken").
void GetTestData(list_t *depts, list_t *emps)
{
  dept_t *d10 = dept_Alloc("Hauptquartier", "New York",     10, false);
  dept_t *d20 = dept_Alloc("Forschung",     "New York",     20, false);
  dept_t *d30 = dept_Alloc("Verkauf",       "New York",     30, false);
  dept_t *d31 = dept_Alloc("Verkauf Ost",   "Boston",       31, false);
  dept_t *d32 = dept_Alloc("Verkauf Nord",  "Seattle",      32, false);
  dept_t *d33 = dept_Alloc("Verkauf West",  "Los Angeles",  33, false);
  dept_t *d34 = dept_Alloc("Verkauf Sued",  "Miami",        34, false);
  dept_t *d40 = dept_Alloc("Betrieb",       "New York",     40, false);
  list_Append(depts, d10);
  list_Append(depts, d20);
  list_Append(depts, d30);
  list_Append(depts, d31);
  list_Append(depts, d32);
  list_Append(depts, d33);
  list_Append(depts, d34);
  list_Append(depts, d40);

  emp_t *e7401 = emp_Alloc("Amanda",    "Scott",      7401, JT_APPRENTICE,  d32, (date_t) { 1961, 12, 13 }, (date_t) { 1981, 12, 01 }, 1300.00, false);
  emp_t *e7402 = emp_Alloc("Susann",    "Fisherman",  7402, JT_APPRENTICE,  d32, (date_t) { 1961, 12, 13 }, (date_t) { 1981, 12, 01 }, 1300.00, false);
  emp_t *e7403 = emp_Alloc("Michael",   "Wood",       7403, JT_APPRENTICE,  d32, (date_t) { 1961, 12, 13 }, (date_t) { 1981, 12, 01 }, 1300.00, false);
  emp_t *e7499 = emp_Alloc("Woody",     "Allen",      7499, JT_EMPLOYEE,    d32, (date_t) { 1961, 12, 13 }, (date_t) { 1981, 12, 01 }, 1600.00, false);
  emp_t *e7501 = emp_Alloc("Samantha",  "Davis",      7501, JT_APPRENTICE,  d31, (date_t) { 1961, 12, 13 }, (date_t) { 1981, 12, 01 }, 1300.00, false);
  emp_t *e7502 = emp_Alloc("Julienne",  "Miller",     7502, JT_APPRENTICE,  d31, (date_t) { 1961, 12, 13 }, (date_t) { 1981, 12, 01 }, 1300.00, false);
  emp_t *e7503 = emp_Alloc("Peter",     "Taylor",     7503, JT_APPRENTICE,  d31, (date_t) { 1961, 12, 13 }, (date_t) { 1981, 12, 01 }, 1300.00, false);
  emp_t *e7521 = emp_Alloc("Edgar",     "Ward",       7521, JT_EMPLOYEE,    d31, (date_t) { 1961, 02, 13 }, (date_t) { 1981, 02, 01 }, 1600.00, false);
  emp_t *e7534 = emp_Alloc("Martha",    "Miller",     7534, JT_APPRENTICE,  d20, (date_t) { 1960, 02, 13 }, (date_t) { 1980, 02, 01 }, 1300.00, false);
  emp_t *e7566 = emp_Alloc("Ellen",     "Jones",      7566, JT_EMPLOYEE,    d20, (date_t) { 1961, 01, 13 }, (date_t) { 1981, 01, 01 }, 3975.00, false);
  emp_t *e7576 = emp_Alloc("Steven",    "Adams",      7576, JT_TRAINEE,     d20, (date_t) { 1960, 12, 13 }, (date_t) { 1980, 12, 01 }, 1100.00, false);
  emp_t *e7601 = emp_Alloc("Terence",   "Clark",      7601, JT_APPRENTICE,  d33, (date_t) { 1961, 12, 13 }, (date_t) { 1981, 12, 01 }, 1300.00, false);
  emp_t *e7602 = emp_Alloc("Eva",       "Gardner",    7602, JT_APPRENTICE,  d33, (date_t) { 1961, 12, 13 }, (date_t) { 1981, 12, 01 }, 1300.00, false);
  emp_t *e7603 = emp_Alloc("Robert",    "Jones",      7603, JT_APPRENTICE,  d33, (date_t) { 1961, 12, 13 }, (date_t) { 1981, 12, 01 }, 1300.00, false);
  emp_t *e7654 = emp_Alloc("Ben",       "Martin",     7654, JT_EMPLOYEE,    d33, (date_t) { 1960, 02, 13 }, (date_t) { 1980, 02, 01 }, 1600.00, false);
  emp_t *e7699 = emp_Alloc("Harry",     "Higgins",    7699, JT_EMPLOYEE,    d30, (date_t) { 1961, 01, 13 }, (date_t) { 1981, 01, 01 }, 4000.00, false);
  emp_t *e7782 = emp_Alloc("Suzanne",   "Clark",      7782, JT_EMPLOYEE,    d40, (date_t) { 1962, 01, 13 }, (date_t) { 1982, 01, 01 }, 3450.00, false);
  emp_t *e7788 = emp_Alloc("Robert",    "Scott",      7788, JT_EMPLOYEE,    d20, (date_t) { 1963, 01, 13 }, (date_t) { 1983, 01, 01 }, 3000.00, false);
  emp_t *e7801 = emp_Alloc("Harry",     "Farnham",    7801, JT_APPRENTICE,  d34, (date_t) { 1961, 03, 13 }, (date_t) { 1981, 03, 01 }, 1300.00, false);
  emp_t *e7802 = emp_Alloc("Steven",    "Wiggins",    7802, JT_APPRENTICE,  d34, (date_t) { 1961, 04, 13 }, (date_t) { 1981, 04, 01 }, 1300.00, false);
  emp_t *e7803 = emp_Alloc("Eliza",     "Dolittle",   7803, JT_APPRENTICE,  d34, (date_t) { 1962, 04, 13 }, (date_t) { 1982, 04, 01 }, 1300.00, false);
  emp_t *e7839 = emp_Alloc("Stephen",   "King",       7839, JT_MANAGER,     d10, (date_t) { 1960, 01, 13 }, (date_t) { 1980, 01, 01 }, 5000.00, false);
  emp_t *e7844 = emp_Alloc("Tina",      "Turner",     7844, JT_EMPLOYEE,    d34, (date_t) { 1961, 02, 13 }, (date_t) { 1981, 02, 01 }, 1600.00, false);
  emp_t *e7898 = emp_Alloc("Adam",      "Blake",      7898, JT_EMPLOYEE,    d10, (date_t) { 1961, 01, 13 }, (date_t) { 1981, 01, 01 }, 4000.00, false);
  emp_t *e7902 = emp_Alloc("Henry",     "Ford",       7902, JT_EMPLOYEE,    d40, (date_t) { 1962, 01, 13 }, (date_t) { 1982, 01, 01 }, 3000.00, false);
  emp_t *e7903 = emp_Alloc("John",      "James",      7903, JT_TRAINEE,     d40, (date_t) { 1960, 03, 13 }, (date_t) { 1980, 03, 01 },  950.00, false);
  emp_t *e7969 = emp_Alloc("John",      "Smith",      7969, JT_TRAINEE,     d40, (date_t) { 1960, 02, 13 }, (date_t) { 1980, 02, 01 },  800.00, false);
  list_Append(emps, e7401);
  list_Append(emps, e7402);
  list_Append(emps, e7403);
  list_Append(emps, e7499);
  list_Append(emps, e7501);
  list_Append(emps, e7502);
  list_Append(emps, e7503);
  list_Append(emps, e7521);
  list_Append(emps, e7534);
  list_Append(emps, e7566);
  list_Append(emps, e7576);
  list_Append(emps, e7601);
  list_Append(emps, e7602);
  list_Append(emps, e7603);
  list_Append(emps, e7654);
  list_Append(emps, e7699);
  list_Append(emps, e7782);
  list_Append(emps, e7788);
  list_Append(emps, e7801);
  list_Append(emps, e7802);
  list_Append(emps, e7803);
  list_Append(emps, e7839);
  list_Append(emps, e7844);
  list_Append(emps, e7898);
  list_Append(emps, e7902);
  list_Append(emps, e7903);
  list_Append(emps, e7969);
}

// Benutzereingaben hinzufügen.
void GetUserInput(list_t *depts, list_t *emps)
{
  dept_t *dept;
  emp_t *emp;
  // J/N/<ESC>
  while ((char)GetOption("jn\x1B", "Eine Abteilung hinzufuegen (j/n)? ") == 'j')
  {
    printf("\n");
    dept = MALLOC(dept_t);
    *dept = dept_GetFromConsole();
    list_Append(depts, dept);
    // J/N/<ESC>
    while ((char)GetOption("jn\x1B", "Der Abteilung einen Angestellten hinzufuegen (j/n)? ") == 'j')
    {
      printf("\n");
      emp = MALLOC(emp_t);
      *emp = emp_GetFromConsole(dept);
      dept_AddEmp(dept, emp);
      list_Append(emps, emp);
    }
    printf("\n");
  }
  printf("\n\n");
}

cmp_t CompareEmpLastName(emp_t *left, emp_t *right)
{
  int cmp = _strcmpi(left->last_name, right->last_name);
  if (cmp < 0)
  {
    return CMP_LESS;
  }
  else if (cmp > 0)
  {
    return CMP_GREATER;
  }
  return CMP_EQUAL;
}

void TestDept(dept_t *dept)
{
  // Angestellte nach Nachname sortieren.
  list_Sort(&dept->emps, CompareEmpLastName, true);

  dept_PutToConsole(dept);

  const emp_t *const emp_age = dept_GetEmpByMaxAge(dept);
  if (emp_age == NULL)
  {
    printf("  Diese Abteilung hat keine Mitarbeiter.");
  }
  else
  {
    printf("  Der aelteste Mitarbeiter: %s %s\n", emp_age->first_name, emp_age->last_name);

    const emp_t *const emp_sen = dept_GetEmpByMaxSeniority(dept);
    if (emp_sen != NULL)
    {
      printf("  Der dienstaelteste Mitarbeiter: %s %s\n", emp_sen->first_name, emp_sen->last_name);
    }

    const emp_t *const emp_mgr = dept_GetManager(dept);
    if (emp_mgr == NULL)
    {
      printf("  Diese Abteilung hat keinen eigenen Manager.\n");
    }
    else
    {
      printf("  Der Manager dieser Abteilung ist: %s %s\n", emp_mgr->first_name, emp_mgr->last_name);
    }

    printf("  Das in dieser Abteilung gezahlte Gesamtgehalt: %0.02lf Euro\n", dept_GetSalSum(dept));
  }
  printf("\n");
}

/* Testwerte (mit Zeilenumbrüchen auf die Ausgabe kopieren):
j
Test
Baton Rouge
66
j
Max
Power
9999
1
06.06.1981
06.06.2008
2345.67
j
Peter
Griffin
7777
1
02.02.1980
12.12.2012
1234.56
n
n
*/
void Test()
{
  list_t depts, emps;
  list_Ctor(&depts, dept_Free);
  list_Ctor(&emps, emp_Free);

  GetTestData(&depts, &emps);
  GetUserInput(&depts, &emps);

  list_ForEach(&depts, TestDept);

  list_Dtor(&emps); 
  list_Dtor(&depts);

  Pause();
}
