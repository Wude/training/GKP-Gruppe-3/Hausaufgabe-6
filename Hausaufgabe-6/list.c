#include "stdafx.h"

#include "list.h"

void list_Ctor(list_t *list, action_fn on_node_free)
{
  list->first = NULL;
  list->last = NULL;
  list->count = 0;
  list->on_node_free = on_node_free;
}

void list_Prepend(list_t *list, const void *const data)
{
  list_node_t *node = MALLOC(list_node_t);
  node->data = (void*)data;
  node->prev = NULL;
  node->next = list->first;

  if (list->first == NULL)
  {
    list->first = node;
  }
  else if (list->first != NULL)
  {
    list->first->prev = node;
  }
  list->first = node;
  list->count++;
}

void list_Append(list_t *list, const void *const data)
{
  list_node_t *node = MALLOC(list_node_t);
  node->data = (void*)data;
  node->prev = list->last;
  node->next = NULL;

  if (list->first == NULL)
  {
    list->first = node;
  }
  else if (list->last != NULL)
  {
    list->last->next = node;
  }
  list->last = node;
  list->count++;
}

void list_InsertBefore(list_t *list, list_node_t *node, const void *const data)
{
  if (node == NULL)
  {
    list_Prepend(list, data);
  }
  else
  {
    list_node_t *new_node = MALLOC(list_node_t);
    new_node->prev = node->prev;
    new_node->next = node;
    new_node->data = (void*)data;

    if (node->prev != NULL)
    {
      node->prev->next = new_node;
    }
    node->prev = new_node;

    if (list->first == node)
    {
      list->first = new_node;
    }
    list->count++;
  }
}

void list_InsertAfter(list_t *list, list_node_t *node, const void *const data)
{
  if (node == NULL)
  {
    list_Append(list, data);
  }
  else
  {
    list_node_t *new_node = MALLOC(list_node_t);
    new_node->prev = node;
    new_node->next = node->next;
    new_node->data = (void*)data;

    if (node->next != NULL)
    {
      node->next->prev = new_node;
    }
    node->next = new_node;

    if (list->last == node)
    {
      list->last = new_node;
    }
    list->count++;
  }
}

list_node_t *list_GetFirstNode(const list_t *const list)
{
  return list->first;
}

list_node_t *list_GetLastNode(const list_t *const list)
{
  return list->last;
}

list_node_t *list_GetNodeByIndex(const list_t *const list, size_t index)
{
  list_node_t *node = list->first;

  for (size_t i = 1; i <= index && node != NULL; i++)
  {
    node = node->next;
  }
  return node;
}

list_node_t *list_node_GetPrev(const list_node_t *const node)
{
  if (node == NULL)
  {
    return NULL;
  }
  return node->prev;
}

list_node_t *list_node_GetNext(const list_node_t *const node)
{
  if (node == NULL)
  {
    return NULL;
  }
  return node->next;
}

void list_node_GetData(const list_node_t *const node, void **data)
{
  if (node == NULL)
  {
    *data = NULL;
  }
  else
  {
    *data = node->data;
  }
}

list_node_t *list_GetNodeByData(const list_t *const list, void *data)
{
  void *node_data;
  for (
    list_node_t *node = list_GetFirstNode(list);
    node != NULL;
    node = list_node_GetNext(node))
  {
    list_node_GetData(node, &node_data);
    if (data == node_data)
    {
      return node;
    }
  }
  return NULL;
}

void list_ForEach(const list_t *const list, action_fn action)
{
  if (action != NULL)
  {
    void *data;
    for (
      list_node_t *node = list_GetFirstNode(list);
      node != NULL;
      node = list_node_GetNext(node))
    {
      list_node_GetData(node, &data);
      action(data);
    }
  }
}

list_node_t *Split(list_node_t *left, size_t count)
{
  list_node_t *right = left;
  for (size_t i = 0; i < count; i++)
  {
    right = right->next;
  }
  if (right->prev != NULL)
  {
    right->prev->next = NULL;
    right->prev = NULL;
  }
  return right;
}

list_node_t *Merge(list_node_t *left, list_node_t *right, compare_fn compare, cmp_t cmp_greater)
{
  list_node_t *node;

  // Ersten Knoten bestimmen.
  if (compare(left->data, right->data) == cmp_greater)
  {
    node = right;
    right = right->next;
  }
  else
  {
    node = left;
    left = left->next;
  }
  list_node_t *current = node;

  for (;;)
  {
    // Wenn eine der Teillisten aufgebraucht ist,
    // kann die Verschmelzung abgekürzt werden.
    if (left == NULL)
    {
      right->prev = current;
      current->next = right;
      break;
    }
    else if (right == NULL)
    {
      left->prev = current;
      current->next = left;
      break;
    }

    // Je nach Vergleichsergebnis die Zielliste erweitern.
    if (compare(left->data, right->data) == cmp_greater)
    {
      right->prev = current;
      current->next = right;
      current = right;
      right = right->next;
    }
    else
    {
      left->prev = current;
      current->next = left;
      current = left;
      left = left->next;
    }
  }

  return node;
}

list_node_t *Sort(list_node_t *left, size_t count, compare_fn compare, cmp_t cmp_greater)
{
  if (count <= 1)
  {
    return left;
  }

  list_node_t *right;
  size_t half_count = count / 2;

  // Liste in etwa gleichgroße Teile aufspalten.
  right = Split(left, half_count);

  // Die Teillisten sortieren.
  left = Sort(left, half_count, compare, cmp_greater);
  right = Sort(right, count - half_count, compare, cmp_greater);

  // Die sortierten Teillisten wieder verschmelzen.
  return Merge(left, right, compare, cmp_greater);
}

void list_Sort(list_t *list, compare_fn compare, bool asc)
{
  const cmp_t cmp_greater = ((asc) ? 1 : -1);
  list->first = Sort(list->first, list->count, compare, cmp_greater);
  list_node_t *node;
  for (node = list->first; node != NULL; node = node->next)
  {
  }
  list->last = node;
}

void list_Remove(list_t *list, list_node_t *node)
{
  if (list->first == node)
  {
    list->first = node->next;
  }
  else if (node->prev != NULL)
  {
    node->prev->next = node->next;
  }
  if (list->last == node)
  {
    list->last = node->prev;
  }
  else if (node->next != NULL)
  {
    node->next->prev = node->prev;
  }
  list->count--;
  if (list->on_node_free != NULL)
  {
    list->on_node_free(node);
  }
  free(node);
}

void list_Truncate(list_t *list)
{
  list_node_t *node = list->first;
  list_node_t *next;
  action_fn on_node_free = list->on_node_free;
  if (on_node_free == NULL)
  {
    while (node != NULL)
    {
      next = node->next;
      free(node);
      node = next;
    }
  }
  else
  {
    while (node != NULL)
    {
      next = node->next;
      on_node_free(node->data);
      free(node);
      node = next;
    }
  }

  list->first = NULL;
  list->last = NULL;
  list->count = 0;
}

void list_Dtor(list_t *list)
{
  list_Truncate(list);
}
