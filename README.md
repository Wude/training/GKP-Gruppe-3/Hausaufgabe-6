# Hausaufgabe 6

## Aufgabe 1: Abteilungsverwaltung (49 + 6 Punkte)

### i) Verbund für Abteilung (4 Punkte)

Erstellen Sie eine Datei abteilung.h, in der Sie die Struktur einer Abteilung definieren.
Eine Abteilung soll dabei durch die folgenden Eigenschaften definiert sein:

* Name (Zeichenkette)
* Adresse (Zeichenkette)
* Abteilungsnummer (Ganzzahl)
* Liste der Angestellten der Abteilung (Verkette Liste)

Verwenden Sie für die Liste der Angestellte Ihre in den letzten Übungen erstellte Implementierung
einer verketteten Liste bzw. schauen Sie für die Umsetzung in die Vorlesungsfolien.

### ii) Verbund für Angestellte (4 Punkte)

Erstellen Sie eine Datei angestellter.h, in welcher Sie eine Struktur für einen Angestellten
mit den folgenden Eigenschaften anlegen:
* Vorname und Nachname (je eine Zeichenkette)
* Personalnummer (Ganzzahl)
* Position (Aufzählungstyp, Werte s.u.)
* Abteilung (Pointer auf Abteilung -> s. Aufgabe i)
* Geburtsdatum (Typ Datum -> letzte Hausaufgabe)
* Einstellungsdatum (Typ Datum -> letzte Hausaufgabe)
* Gehalt (Gleitkommazahl)

Der Aufzählungstyp für die Position des Angestellten soll die folgenden Werte umfassen:

* Leiter
* Mitarbeiter
* Azubi
* Praktikant

### iii) Funktionen für Abteilungen (25 Punkte)

Ergänzen Sie die Datei abteilung.h um die Definition der folgenden Funktionen:

* Eine Funktion, welche das Erstellen einer neuen Abteilung ermöglicht. Die Funktion soll interaktiv vom Nutzer Werte für Name, Adresse und Abteilungsnummer abfragen und eine entsprechende Abteilung erstellen.
* Eine Funktion zum Hinzufügen eines neuen Angestellten zur Abteilung.
* Eine Funktion zum Entfernen eines Angestellten aus einer Abteilung.
* Eine Funktion, welche einen Angestellten von einer Abteilung A in eine Abteilung B verschiebt.
* Eine Funktion zur Berechnung des Gesamtgehaltes der Abteilung (sprich die Summe aller Gehälter der Angestellten einer Abteilung).
* Eine Funktion, welche den Angestellten mit der längsten Betriebszugehörigkeit zurück gibt.
* Eine Funktion, welche den (kalendarisch) ältesten Angestellten der Abteilung zurück gibt.
* Eine Funktion, die den Leiter der Abteilung ermittelt.
* Eine Funktion zur Ausgabe einer kompletten Abteilung inkl. aller Angestellten.

Implementieren Sie diese Funktionen in einer Datei namens abteilung.(c|cpp).
Wichtig: Stellen Sie in Ihrer Implementierung sicher, dass eine Abteilung maximal einen
Angestellten mit der Position Leiter besitzt.
Hinweis: Bevor Sie die oben gelisteten Funktionen implementieren, werfen Sie auch einen
Blick auf Aufgabe iv) - ggf. finden sich dort Funktionen, welche Ihnen bei der Umsetzung
helfen.

### iv) Funktionen für Angestellte (8 Punkte)

Ergänzen Sie die Datei angestellter.h um die Definition der folgenden Funktionen:

* Eine Funktion, welche das Erstellen eines neuen Angestellten ermöglicht. Die Funktion soll dabei interaktiv die Werte für Vorname und Nachname, Personalnummer, Position, Geburts- und Einstellungsdatum sowie Gehalt abfragen.
* Eine Funktion, welche das Alter des Angestellten in Jahren zurück gibt.
* Eine Funktion, welche die Betriebszugehörigkeit des Angestellten in Jahren zurück gibt.

Implementieren Sie diese Funktionen in einer Datei namens angestellter.(c|cpp).

### v) Testprogramm (8 Punkte)

Schreiben Sie ein Programm, um die in den Teilaufgaben
iii) und iv) geschrieben Funktionen zu testen. Dieses sollte zum Test der Eingabefunktion
zumindest teilweise interaktiv sein. Die Prüfung der anderen Funktionen mit zur
Compilezeit bekannten (Grenz-)Werten ist ausreichend.

### vi) Zusatzaufgabe: Sortierte Ausgabe (6 Punkte)

Passen Sie die Ausgabe einer Abteilung dahingehend an,
dass die Liste der Angestellten nach Nachname (aufsteigend)
sortiert ausgegeben wird. Überlegen Sie sich dabei, ob Sie dafür die Ausgabe-Funktion
oder die Speicherung der Angestellten in der Liste anpassen.
